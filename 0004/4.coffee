Number::isPalindrome = ->
	str = "#{this}"
	for c, i in str
		return no if c isnt str[str.length - i - 1]
	yes

best = ->

	result = 0

	i = 1000
	while (--i > 100)
		j = i
		while (j-- > 100)
			product = i * j
			if product.isPalindrome()
				result = Math.max(result, product)

	return result

console.log best()
