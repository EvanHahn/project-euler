const answer = (2n ** 1000n)
  .toString()
  .split("")
  .map(Number)
  .reduce((a, b) => a + b);

console.log(answer);
