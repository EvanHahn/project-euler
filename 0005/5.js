var UPPER_BOUND = 20;

Number.prototype.isMultipleOf = function(n) {
  return (this % n) === 0;
};

function answer() {
	for (var i = 0; true; i += UPPER_BOUND) {
		var multiple = true;
		for (var j = UPPER_BOUND; multiple && (j >= 1); j --) {
			multiple = multiple && i.isMultipleOf(j);
		}
		if (multiple)
			return i;
	}
}

console.log(answer());
