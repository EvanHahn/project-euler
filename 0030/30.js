var POWER = 5;
var STOP_AFTER = 9999999;

function sumDigits(n) {
	var str = n.toString();
	if (str.length < 2)
		return 0;
	var sum = 0;
	for (var i = 0; i < str.length; i ++)
		sum += Math.pow(parseInt(str[i]), POWER);
	return sum;
}

var sum = 0;

for (var i = 2; i < STOP_AFTER; i ++) {

	if (i === sumDigits(i)) {
		console.log(i);
		sum += i;
	}

}

console.log('sum: ' + sum);