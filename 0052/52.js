require('sugar');

var MIN = 2;
var MAX = 6;

function sort(n) {
	return (n).toString().chars().sort().join('');
}

var answer;
for (var x = 1; answer == null; x ++) {

	var good = true;

	var firstSorted = sort(MIN * x);

	for (var i = MIN; i <= MAX; i ++) {
		var sorted = sort(i * x);
		if (sorted != firstSorted) {
			good = false;
			break;
		}
	}

	if (good)
		answer = x;

}

console.log(answer);
