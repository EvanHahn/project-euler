RANGE = [1..100]

sumOfSquares = RANGE.reduce (v, n) -> v + (n * n)
rangeSum = eval(RANGE.join("+"))
squareOfSum = rangeSum * rangeSum

console.log(squareOfSum - sumOfSquares)
