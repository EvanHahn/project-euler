#define SUM 1000

#include <math.h>
#include <vector>
#include <iostream>

using namespace std;

bool is_square(int n) {
	double root = sqrt((double) n);
	return floor(root) == root;
}

int main() {

	vector<int> squares;
	for (int i = 1; i <= (SUM * SUM); i ++) {
		if (is_square(i))
			squares.push_back(i);
	}

	int a2, b2, c2, a, b, c;
	for (int i = 0; i < squares.size(); i ++) {
		for (int j = 0; j < squares.size(); j ++) {
			for (int k = 0; k < squares.size(); k ++) {

				a2 = squares[i];
				b2 = squares[j];
				c2 = squares[k];

				if ((a2 + b2) == c2) {

					a = (int) sqrt(a2);
					b = (int) sqrt(b2);
					c = (int) sqrt(c2);
					
					if ((a + b + c) == SUM)
						cout << (a * b * c) << endl;

				}

			}
		}
	}

}
