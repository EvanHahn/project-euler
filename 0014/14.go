package main
import "fmt"

func chainSize(n int) int {

	count := 1

	for n != 1 {
		if (n % 2) == 0 {
			n /= 2
		} else {
			n = (3 * n) + 1
		}
		count += 1
	}

	return count

}

func main() {

	var longestStart int
	longestChain := 0

	for i := 1; i < 1000000; i += 1 {
		size := chainSize(i)
		if size > longestChain {
			longestChain = size
			longestStart = i
		}
	}

	fmt.Println(longestStart)

}
