MAX_NUMBER = 4000000 - 1

# self-caching Fibonacci stolen from:
# http://davidchambersdesign.com/self-caching-functions-in-javascript-and-python/
fib = do ->
	cache = {}
	return (n) ->
		cached = cache[n]
		return cached if cached?
		return n if n <= 1
		return cache[n] = fib(n - 2) + fib(n - 1)

Number::even = ->
	(this % 2) == 0

sum = 0
result = fib(1)
i = 1
while result <= MAX_NUMBER
	sum += result if result.even()
	i++
	result = fib(i)

console.log sum
