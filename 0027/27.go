package main

import (
	"fmt"
)

const MAX int = 1000

func isPrime(n int) bool {
	if n <= 1 { return false }
	for i := 2; i < n; i ++ {
		if (n % i) == 0 { return false }
	}
	return true
}

func main() {

	var (
		bestCount int
		bestA int
		bestB int
	)

	for a := -MAX; a < MAX; a ++ {
		for b := -MAX; b < MAX; b ++ {

			count := 0
			stillPrime := true

			for n := 0; stillPrime; n ++ {
				value := (n * n) + (a * n) + b
				stillPrime = isPrime(value)
				if stillPrime { count ++ }
			}

			if count > bestCount {
				bestCount = count
				bestA = a
				bestB = b
			}

		}
	}

	fmt.Println("a =", bestA, "and b =", bestB, "creating", bestCount, "primes")
	fmt.Println("a * b =", bestA * bestB)

}
