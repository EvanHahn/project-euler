package main
import "fmt"

const MAX = 2000000

func primer() func(int) bool {

	var primes [MAX] int
	primesCount := 0

	return func(n int) bool {

		if n <= 1 { return false }

		for i := 0; i < primesCount; i += 1 {
			if (n % primes[i]) == 0 {
				return false
			}
		}

		primes[primesCount] = n
		primesCount += 1
		return true

	}

}


func main() {

	isPrime := primer()

	sum := 0

	for i := 1; i <= MAX; i++ {
		if (isPrime(i)) {
			sum += i
		}
	}

	fmt.Println(sum)

}
