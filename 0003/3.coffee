Number::isMultipleOf = (n) ->
	(this % n) == 0

Number::factors = ->
	result = []
	sqrt = Math.ceil(Math.sqrt(this))
	for n in [1..sqrt]
		if this.isMultipleOf n
			result.push(n)
			result.push(this / n) if n isnt sqrt
	result

primeCache = []
Number::isPrime = ->
	unless primeCache[this]?
		primeCache[this] = @factors().length is 2
	primeCache[this]

Number::largestPrimeFactor = ->
	factors = @factors()
	while factors.length
		factor = factors.pop()
		return factor if factor.isPrime()

console.log 600851475143.largestPrimeFactor()
