MULTIPLES_OF = [3, 5]
MAX_NUMBER = 999

Number::multipleOf = (n) ->
	(this % n) == 0

sum = 0
for n in [1..MAX_NUMBER]
	isMultiple = no
	for m in MULTIPLES_OF
		isMultiple or= n.multipleOf m
	sum += n if isMultiple

console.log sum
