require('sugar');

function alphaValue(word) {
	return word.toUpperCase().codes().sum(function(n) {
		return n - 64;
	});
}

var names = require('./names').names;

names.sort();

var answer = names.sum(function(name, index) {
	return alphaValue(name) * (index + 1);
});

console.log(answer);
