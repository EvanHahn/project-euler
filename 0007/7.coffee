Number::isMultipleOf = (n) ->
	(this % n) == 0

Number::factors = ->
	result = []
	sqrt = Math.ceil(Math.sqrt(this))
	for n in [1..sqrt]
		if this.isMultipleOf n
			result.push(n)
			result.push(this / n) if n isnt sqrt
	result

primeCache = [[], [1], [1,2], [1,3]]
Number::isPrime = ->
	unless primeCache[this]?
		primeCache[this] = @factors().length is 2
	primeCache[this]

primeCount = -1  # accounts for 1 being a prime
i = 0
while (primeCount < 10001) and (++i)
	primeCount++ if i.isPrime()

console.log i
