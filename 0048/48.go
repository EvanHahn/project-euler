package main
import (
	"fmt"
	"math"
)

const (
	MAX uint64 = 1000
	DIGITS float64 = 10
)

func chop(n uint64) uint64 {
	return n % uint64(math.Pow(10, DIGITS))
}

func powpow(n uint64) uint64 {
	var (
		result uint64
		i uint64
	)
	result = 1
	for i = 0; i < n; i ++ {
		result = chop(result * n)
	}
	return result
}

func main() {

	var (
		sum uint64
		i uint64
	)
	sum = 0
	for i = 1; i <= MAX; i ++ {

		if ((i % 10) != 0) {
			sum += powpow(i)
			sum = chop(sum)
		}

	}

	fmt.Println(sum)

}
