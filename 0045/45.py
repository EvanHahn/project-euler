from math import floor, sqrt

def triangle(n):
    return (n * (n + 1)) / 2

def is_integer(n):
    return floor(n) == n

def is_pentagonal(n):
    original = (sqrt((24 * n) + 1) + 1) / 6
    return is_integer(original)

def is_hexagonal(n):
    original = (sqrt((8 * n) + 1) + 1) / 4
    return is_integer(original)

for i in range(100000):
    t = triangle(i)
    if is_pentagonal(t) and is_hexagonal(t):
        print "T[" + str(i) + "] is all three"
