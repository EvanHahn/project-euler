START_DATE = Date.parse('1 January 1901')
END_DATE = Date.parse('31 December 2000')

total = 0

day = START_DATE
while day != END_DATE do

  if (day.mday == 1) and (day.sunday?)
    total += 1
  end

  day = day.next_day

end

puts total
