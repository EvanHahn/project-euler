#include <stdio.h>
#include <math.h>

#define SIZE 1001
#define bool char

int main() {

	int sum;
	int i;
	int squared;

	sum = 1;

	for (i = 3; i <= SIZE; i += 2) {

		squared = i * i;
		sum += (4 * squared) - (6 * i) + 6;

	}

	printf("sum = %d\n", sum);

	return 0;

}
