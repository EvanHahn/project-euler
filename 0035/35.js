var MAX = 1E6;

var primes = [];

function generatePrimes() {

	function checkPrime(n) {
		for (var i = 0, len = primes.length; i < len; i ++) {
			if ((n % primes[i]) === 0)
				return;
		}
		primes.push(n);
	}

	for (var i = 2; i < MAX; i ++)
		checkPrime(i);

}

function isPrime(n) {
	return primes.indexOf(n) !== -1;
}

function rotate(n, count) {
	var str = n.toString();
	for (var i = 0; i < count; i ++)
		str = str.substr(1) + str[0];
	return parseInt(str, 10);
}

generatePrimes();
console.log('primes below ' + MAX + ' generated');

var result = [];

for (var i = 0, len = primes.length; i < len; i ++) {

	var good = true;

	var original = primes[i];
	var copy;
	for (var rotations = 1; copy !== original; rotations ++) {
		copy = rotate(original, rotations);
		if (!isPrime(copy)) {
			good = false;
			break;
		}
	}

	if (good) {
		result.push(original);
		console.log(original + ' (#' + (i + 1) + ') is circular');
	}

}

// console.log(result);
console.log(result.length + ' results');
