#define DESIRED_FACTORS 500

#include <stdio.h>
#include <math.h>

int factor_count(int n) {
	int i, count;
	count = 0;
	for (i = 1; i <= (int) sqrt(n); i ++) {
		if ((n % i) == 0) {
			count ++;
			if (i != ((int) sqrt(n)))
				count ++;
		}
	}
	return count;
}

int main() {
	int n, i;
	n = 0;
	for (i = 1; factor_count(n) < DESIRED_FACTORS; i ++) {
		printf("%d with %d factors\n", n, factor_count(n));
		n += i;
	}
	printf("%d\n", n);
}
