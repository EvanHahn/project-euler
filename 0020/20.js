function factorial(n) {
  let result = 1n;
  for (let i = n; i > 0; i--) {
    result *= i;
  }
  return result;
}

function sumdigits(n) {
  return n
    .toString()
    .split("")
    .map(Number)
    .reduce((a, b) => a + b);
}

console.log(sumdigits(factorial(100n)));
