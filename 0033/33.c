#include <stdio.h>

short ones(short n) {
	return n % 10;
}

short tens(short n) {
	return (n - ones(n)) / 10;
}

int main() {

	short num;
	short den;

	for (den = 11; den < 100; den ++) {
		for (num = 11; num < den; num ++) {

			char is_trivial = (ones(num) == 0) && (ones(den) == 0);
			if (is_trivial)
				continue;

			float old_value = ((float) num) / ((float) den);

			float tn = (float) tens(num);
			float on = (float) ones(num);
			float td = (float) tens(den);
			float od = (float) ones(den);

			char is_answer =
				((on == td) && ((tn / od) == old_value));

			if (is_answer)
				printf("%d / %d\n", num, den);

		}
	}

	return 0;

}

