MAX = 1e6

String::isPalindrome = ->
  for c, i in this
    return false if c isnt this[@length - i - 1]
  return true

sum = 0

for i in [1..MAX]
  base10 = i.toString(10)
  base2 = i.toString(2)
  if base10.isPalindrome() and base2.isPalindrome()
    sum += i

console.log sum
